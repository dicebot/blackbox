module tests.test2;

unittest
{
    import std.file;
    import std.algorithm;
    import std.array;

    auto entries = dirEntries(".", SpanMode.shallow)
        .map!(entry => entry.name)
        .filter!(name => name == "./expect_me")
        .array();

    assert (entries.length == 1);
}
