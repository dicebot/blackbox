module tests.test1;

unittest
{
    import std.net.curl;
    auto html = get("127.0.0.1:8000/");

    import std.regex;
    static rgx = regex(`>expect_me/`, "g");
    assert(!html.matchFirst(rgx).empty);
}
