import blackbox.tester;

// Any test failure will result in a thrown exception, no special handling is
// done. Sandbox folder can be inspected manually after tests finish, it will
// only get deleted on next run.
void main ()
{
    TestConfiguration config;

    config.sandbox_dir = "tmp";
    config.startup_delay = dur!"msecs"(200);
    config.tested_app = [ "python2", "-m", "SimpleHTTPServer" ];

    config.preparations = {
        import std.file;
        mkdir("expect_me");
    };

    runTests(config);
}
