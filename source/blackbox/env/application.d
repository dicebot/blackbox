/*******************************************************************************

    Wrapper over std.process.spawnProcess

    Forwards all output to file and automatically kills process when wrapper
    struct gets destroyed.

*******************************************************************************/

module blackbox.env.application;

import blackbox.config;

/// ditto
struct TestedApplication {

    import std.process;
    import std.stdio : File;
    import std.experimental.logger;

public:

    @disable this(this);

    /**
        Params:
            config = generic test configuration struct instance. Relevant
                fields are `tested_app`, `env_vars` and `startup_delay`.

        Returns:
            wrapper over running application that can be stopped any time
            by calling `stop` method
     **/
    static TestedApplication start(TestConfiguration config) {

        import core.thread;
        import std.stdio;

        TestedApplication app;

        app.output = File(".app.output", "w");
        app.pid = spawnProcess(config.tested_app, std.stdio.stdin, app.output,
                app.output, config.env_vars, Config.newEnv);
        infof("Started '%s' with pid %s", config.tested_app, app.pid.processID);

        infof("Sleeping for %s", config.startup_delay);
        Thread.sleep(config.startup_delay);

        return app;
    }

    /**
            Stops application process if running
         **/
    void stop() {

        infof("Stopping pid %s", this.pid.processID);

        if (this.pid !is null) {
            kill(this.pid);
            wait(this.pid);
            this.pid = null;
        }

        this.output.close();
    }

package(blackbox):

    Pid pid;
    File output;
}
