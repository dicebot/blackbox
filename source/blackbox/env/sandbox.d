/*******************************************************************************

    Wrapper struct to initialize directory at the specified path in constructor.

    Changes working directory in constructor to sandbox and restores it in
    destructor.

*******************************************************************************/

module blackbox.env.sandbox;

/// ditto
struct Sandbox {

    import std.experimental.logger;
    import std.path;
    import std.file;
    import std.exception;

    import blackbox.config;

    /**
        Params:
            sandbox_dir = relative path to directory to re-create

        Returns:
            wrapper struct which track previous working directory to return
            to on call of `exit` method
     **/
    static Sandbox enter(string sandbox_dir) {

        Sandbox sandbox;
        sandbox.path = sandbox_dir;

        rmdirRecurse(sandbox.path).collectException();
        mkdirRecurse(sandbox.path);

        sandbox.old_wd = getcwd();
        chdir(sandbox.path);

        infof("Initialized sandbox at '%s'", sandbox.path);
        return sandbox;
    }

    /**
        Changes working directory to the one before entering sandbox
     **/
    void exit() {

        infof("Exiting sandbox at '%s'", this.path);
        chdir(this.old_wd);
    }

private:

    string path;
    string old_wd;
}
