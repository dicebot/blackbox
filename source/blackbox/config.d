/*******************************************************************************

    Configuration for the blackbox tester

*******************************************************************************/

module blackbox.config;

public import std.datetime : Duration, dur;

/// ditto
struct TestConfiguration {
    /**
        Path to directory where all the sandbox structure has to be created.
        Relative to the working directory where the test suite binary is run.
     **/
    string sandbox_dir = "sandbox";

    /**
        Path to file where traces from test suite itself should be placed
     **/
    string test_log_path = "./test.log";

    /**
        Amount of time to sleep after starting tested application and before
        executing tests. Usually necessary when testing heavy daemon apps which
        take some time to initialize.
     **/
    Duration startup_delay = dur!"msecs"(1);

    /**
        Command to start tested application (including all arguments)
     **/
    string[] tested_app = ["dmd"];

    /**
        Environment variables to set for tested application. Parent process
        environment is NOT inherited.
     **/
    string[string] env_vars = null;

    /**
        Delegate to be run after sandbox creation to do any user-defined
        preparation before continuing to tests. Working directory is guaranteed
        to be the sandbox root.
     **/
    void delegate() preparations = null;

    /**
        If not empty string, will be used as a filter for module names to
        find tests in. Important for preventing unrelated unittest blocks from
        dependencies to be executed in the sandbox.
     **/
    string test_package = "tests.";
}
