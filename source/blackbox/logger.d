/*******************************************************************************

    Derivative of FileLoger with simplified output format

*******************************************************************************/

module blackbox.logger;

import std.experimental.logger.filelogger;
import std.experimental.logger.core;

import std.exception : collectException;
import std.concurrency : Tid;
import std.datetime : SysTime;
import std.stdio : File;
import std.format : formattedWrite;
import std.file : remove;

/// ditto
class TestLogger : FileLogger {
    private string name;

    /**
        Constructor

        Forwards to FileLogger constructor but also removes log file upon
        initialization.
     **/
    this(string file, const LogLevel lv = LogLevel.all) @safe {
        remove(file).collectException();
        super(file, lv);
    }

    /**
        Params:
            name = test name to use in logging output for follow up messages
     **/
    void setCurrentTestName(string name) {
        this.name = name;
    }

    /**
        Overrides message writer to output nothing but current test module
        as a prefix.
     **/
    override protected void beginLogMsg(string file, int line, string funcName,
            string prettyFuncName, string moduleName,
            LogLevel logLevel, Tid threadId, SysTime timestamp, Logger logger) @safe {
        auto lt = this.file().lockingTextWriter();
        formattedWrite(lt, "[%20s] ", this.name);
    }
}
