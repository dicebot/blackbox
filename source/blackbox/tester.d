/*******************************************************************************

    Main API entry point for writing a test suite

*******************************************************************************/

module blackbox.tester;

public import blackbox.config;

import blackbox.env.sandbox;
import blackbox.env.application;

/**
    Custom test runner

    Wraps each `unittest` block found in the binary so that:
        - directory sandbox gets re-created at the start of every module test
            block
        - tested application gets started automatically for each module with
            test blocks and terminated in the end
        - stdout/stderr of tested application is stored in the sandbox directory
            as ".logs/app.output"
        - own test suite traces are stored in the sandbox directory as
            ".logs/test.log"

    Params:
        config = struct containing parameters that control test runner
            behaviour, see `TestConfiguration`
 **/
void runTests(TestConfiguration config) {

    import blackbox.logger;
    import std.experimental.logger;
    import std.string : startsWith;
    import std.exception : enforce;
    import std.process : tryWait;

    auto logger = new TestLogger(config.test_log_path);
    stdThreadLocalLog = logger;

    foreach (m; ModuleInfo) {
        if (m !is null) {
            if (!m.name.startsWith(config.test_package))
                continue;

            auto fp = m.unitTest;

            if (fp !is null) {
                scope (success)
                    infof("All successful\n");

                logger.setCurrentTestName(m.name);

                auto sandbox = Sandbox.enter(config.sandbox_dir);
                scope (exit)
                    sandbox.exit();

                infof("Calling delegate for sandbox preparation");

                if (config.preparations !is null)
                    config.preparations();

                auto app = TestedApplication.start(config);
                scope (exit)
                    app.stop();

                infof("Running tests");
                enforce(!app.pid.tryWait().terminated);

                try {
                    fp();
                    enforce(!app.pid.tryWait().terminated);
                }
                catch (Throwable e) {
                    info(e.toString());
                    throw e;
                }
            }
        }
    }
}

shared static this() {

    import core.runtime;
    import std.exception;

    // dfmt off
    version (unittest) {}
    else static assert(false,
        "Test suite must be compiled with -unittest flag " ~ "to work");
    // dfmt on

    // disable default test runner
    Runtime.moduleUnitTester = { return true; };
}
